# Rotate images 360 degrees in jQuery

Plugin to simulate 360 degree image rotation in jQuery for an existing slides set.

## How to use
- [ ] HTML
```
<div class="rotate-360" id="rotate-360-container">
    <ul>
        <li><img src="images/0000.jpg"/></li>
        <li><img src="images/0001.jpg"/></li>
        <li><img src="images/0002.jpg"/></li>
        ...
        <li><img src="images/9999.jpg"/></li>
    </ul>
</div>    
```

- [ ] JS
```
$(document).ready(function () {
    $('.rotate-360 ul').rotate360({
        timeout: 85,
        repeat: true,
        autoplay: true
    });
});
```

## License
The software is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
