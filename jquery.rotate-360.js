;(function ($, window, document, undefined) {
	$.fn.rotate360 = function (options) {
		let settings = $.extend({
			timeout: 100,
			autoplay: true,
			repeat: true,
			events: function () {
			}
		}, options);

		function Instance(element) {
			this.element = element;
			this.items = {};
			this.interval = settings.timeout;
			this.currentItemIndex = 0;
		}

		Instance.prototype = {

			init: function () {
				this.items = this.element.children();
				this.items.show();

				this.pic_X = this.element.offset().left;
				this.pic_Y = this.element.offset().top;
				this.pic_W = this.element.width() / 2;
				this.pic_H = this.element.height() / 2;
				this.center_X = this.pic_X + this.pic_W;
				this.center_Y = this.pic_Y + this.pic_H;
				this.movestop = this.pic_W / this.items.length;

				this.setEvents();

				if (settings.autoplay) {
					this.play();
				}
			},

			setEvents: function () {
				let self = this;

				this.element.mouseenter(function (event) {
					self.clearAutoplayInterval();
				}).mouseleave(function (event) {
					self.setInterval();
				}).mousemove(function (event) {
					self.clearAutoplayInterval();

					if (0 >= (event.pageX - self.center_X)) {
						self.toggleItem(event.pageX, event.pageY, 'left')
					} else {
						self.toggleItem(event.pageX, event.pageY)
					}
				});
			},

			play: function () {
				this.clearAutoplayInterval();
				this.setInterval();
			},

			setInterval: function () {
				let self = this;

				this.interval = setInterval(function () {
					if (self.items.length > self.currentItemIndex) {
						self.items.eq(self.currentItemIndex).show().siblings().hide();
						self.currentItemIndex++;
					}
					if (settings.repeat === true && (self.currentItemIndex >= self.items.length)) {
						self.currentItemIndex = 0;
					}
				}, settings.timeout)
			},

			stop: function () {
				this.clearAutoplayInterval();
			},

			clearAutoplayInterval: function () {
				if (this.interval) {
					clearInterval(this.interval);
				}
			},

			toggleItem: function (pageX, pageY, dir) {
				this.currentItemIndex = Math.ceil(Math.abs(pageX - this.center_X) / this.movestop);

				if (dir) {
					this.items.eq(this.currentItemIndex).show().siblings().hide();
					this.currentItemIndex++;
				} else {
					this.items.eq(this.items.length - this.currentItemIndex).show().siblings().hide();
					this.currentItemIndex--;
				}
			}
		};

		return this.each(function () {
			let rotator = new Instance($(this));
			rotator.init();
		});
	};

})(jQuery, window, document);